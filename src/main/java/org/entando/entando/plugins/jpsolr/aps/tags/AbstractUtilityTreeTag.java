package org.entando.entando.plugins.jpsolr.aps.tags;

import com.agiletec.aps.system.common.tree.ITreeNode;
import com.agiletec.aps.system.common.tree.ITreeNodeManager;
import java.util.List;

public class AbstractUtilityTreeTag extends AbstractFacetNavTag {

    private String facetNodeCode;

    protected boolean isSelectedOneChild() {
        ITreeNodeManager facetManager = this.getFacetManager();
        List<String> facets = this.getRequiredFacets();
        for (int i = 0; i < facets.size(); i++) {
            String requiredFacet = facets.get(i);
            ITreeNode facet = facetManager.getNode(requiredFacet);
            if (null != facet) {
                boolean check = this.checkSelectChild(facet, this.getFacetNodeCode(), facetManager);
                if (check) {
                    return true;
                }
            }
        }
        return false;
    }

    protected boolean checkSelectChild(ITreeNode facet, String codeForCheck, ITreeNodeManager facetManager) {
        if (facet.getCode().equals(codeForCheck)) {
            return true;
        }
        ITreeNode parentFacet = facetManager.getNode(facet.getParentCode());
        if (null != parentFacet && !parentFacet.getCode().equals(parentFacet.getParentCode())) {
            return this.checkSelectChild(parentFacet, codeForCheck, facetManager);
        }
        return false;
    }

    public String getFacetNodeCode() {
        return facetNodeCode;
    }

    public void setFacetNodeCode(String facetNodeCode) {
        this.facetNodeCode = facetNodeCode;
    }

}
