package org.entando.entando.plugins.jpsolr.aps.system.content.widget;

import com.agiletec.aps.BaseTestCase;
import com.agiletec.aps.system.RequestContext;
import com.agiletec.aps.system.SystemConstants;
import com.agiletec.aps.system.common.entity.model.SmallEntityType;
import com.agiletec.aps.system.services.group.Group;
import com.agiletec.aps.system.services.page.Widget;
import com.agiletec.aps.util.ApsProperties;
import com.agiletec.plugins.jacms.aps.system.services.content.IContentManager;
import com.agiletec.plugins.jacms.aps.system.services.searchengine.ICmsSearchEngineManager;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.entando.entando.aps.system.services.searchengine.FacetedContentsResult;
import org.entando.entando.aps.system.services.widgettype.IWidgetTypeManager;
import org.entando.entando.aps.system.services.widgettype.WidgetType;
import org.entando.entando.plugins.jpsolr.aps.system.PluginBaseTestCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FacetNavHelperIntegrationTest extends PluginBaseTestCase {

    private IFacetNavHelper facetNavHelper;

    @BeforeEach
    protected void init() throws Exception {
        try {
            this.facetNavHelper = PluginBaseTestCase.getApplicationContext().getBean(IFacetNavHelper.class);
            ICmsSearchEngineManager searchEngineManager = PluginBaseTestCase.getApplicationContext().getBean(ICmsSearchEngineManager.class);
            Thread thread = searchEngineManager.startReloadContentsReferences();
            thread.join();
        } catch (Exception e) {
            throw e;
        }
    }

    @Test
    public void testResult_1() throws Exception {
        RequestContext reqCtx = this.createRequestContext("admin", null);
        FacetedContentsResult result = this.facetNavHelper.getResult(null, reqCtx);
        Assertions.assertEquals(24, result.getContentsId().size());
    }

    @Test
    public void testResult_2() throws Exception {
        RequestContext reqCtx = this.createRequestContext(SystemConstants.GUEST_USER_NAME, "EVN");
        FacetedContentsResult result = this.facetNavHelper.getResult(null, reqCtx);
        List<String> expectedFreeContentsId = Arrays.asList(new String[]{"EVN191", "EVN192", "EVN193",
                "EVN194", "EVN20", "EVN21", "EVN23", "EVN24", "EVN25"});
        Assertions.assertEquals(expectedFreeContentsId.size(), result.getContentsId().size());
        for (int i = 0; i < expectedFreeContentsId.size(); i++) {
            Assertions.assertTrue(expectedFreeContentsId.contains(result.getContentsId().get(i)));
        }
    }

    @Test
    public void testResult_3() throws Exception {
        RequestContext reqCtx = this.createRequestContext("admin", "ART");
        List<String> categories1 = Arrays.asList(new String[]{"general_cat1"});
        FacetedContentsResult result = this.facetNavHelper.getResult(categories1, reqCtx);
        List<String> expectedFreeContentsId = Arrays.asList(new String[]{"ART102","ART111"});
        Assertions.assertEquals(expectedFreeContentsId.size(), result.getContentsId().size());
        for (int i = 0; i < expectedFreeContentsId.size(); i++) {
            Assertions.assertTrue(expectedFreeContentsId.contains(result.getContentsId().get(i)));
        }
        List<String> categories2 = Arrays.asList(new String[]{"general_cat1", "general_cat2"});
        FacetedContentsResult result_2 = this.facetNavHelper.getResult(categories2, reqCtx);
        List<String> expectedFreeContentsId_2 = Arrays.asList(new String[]{"ART111"});
        Assertions.assertEquals(expectedFreeContentsId_2.size(), result_2.getContentsId().size());
        for (int i = 0; i < expectedFreeContentsId_2.size(); i++) {
            Assertions.assertTrue(expectedFreeContentsId_2.contains(result_2.getContentsId().get(i)));
        }
    }

    private RequestContext createRequestContext(String username, String csvTypes) throws Exception {
        RequestContext reqCtx = this.getRequestContext();
        this.setUserOnSession(username);
        if (null == csvTypes) {
            IContentManager contentManager = getApplicationContext().getBean(IContentManager.class);
            csvTypes = contentManager.getSmallEntityTypes().stream().map(SmallEntityType::getCode).collect(Collectors.joining(","));
        }
        IWidgetTypeManager widgetTypeManager = getApplicationContext().getBean(IWidgetTypeManager.class);
        WidgetType type = widgetTypeManager.getWidgetType("jpsolr_facetResults");
        Widget widget = new Widget();
        widget.setType(type);
        ApsProperties properties = new ApsProperties();
        properties.setProperty("contentTypesFilter", csvTypes);
        widget.setConfig(properties);
        reqCtx.addExtraParam(SystemConstants.EXTRAPAR_CURRENT_WIDGET, widget);
        return reqCtx;
    }

}
