/*
 * Copyright 2015-Present Entando Inc. (http://www.entando.com) All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.entando.entando.plugins.jpsolr.apsadmin.page.specialwidget;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.agiletec.aps.system.SystemConstants;
import com.agiletec.aps.system.services.category.Category;
import com.agiletec.aps.system.services.page.IPage;
import com.agiletec.aps.system.services.page.IPageManager;
import com.agiletec.aps.system.services.page.Widget;
import com.agiletec.apsadmin.system.TreeNodeWrapper;
import com.agiletec.plugins.jacms.aps.system.services.content.model.SmallContentType;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.entando.entando.aps.system.services.widgettype.WidgetTypeParameter;
import org.entando.entando.plugins.jpsolr.apsadmin.ApsAdminPluginBaseTestCase;
import org.entando.entando.plugins.jpsolr.apsadmin.portal.specialwidget.FacetNavTreeWidgetAction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;

public class TestFacetNavTreeWidgetAction extends ApsAdminPluginBaseTestCase {

	private IPageManager pageManager = null;

	@BeforeEach
	private void init() throws Exception {
		try {
			this.pageManager = (IPageManager) this.getService(SystemConstants.PAGE_MANAGER);
		} catch (Throwable t) {
			throw new Exception(t);
		}
	}

	@Test
	public void testInitConfig_1() throws Throwable {
		String result = this.executeConfigFacetNavTree("admin", "homepage", "1", "jpsolr_facetTree");
		assertEquals(Action.SUCCESS, result);
		FacetNavTreeWidgetAction action = (FacetNavTreeWidgetAction) this.getAction();
		Widget widget = action.getWidget();
		assertNotNull(widget);
		assertEquals(0, widget.getConfig().size());
		List<SmallContentType> contentTypes = action.getContentTypes();
		assertNotNull(contentTypes);
		assertEquals(4, contentTypes.size());
	}

	@Test
	public void testInitConfig_2() throws Throwable {
		String result = this.executeConfigFacetNavTree("admin", "homepage", "1", "jpsolr_facetTree");
		assertEquals(Action.SUCCESS, result);
		FacetNavTreeWidgetAction action = (FacetNavTreeWidgetAction) this.getAction();
		action.setTreeNodeActionMarkerCode(FacetNavTreeWidgetAction.ACTION_MARKER_OPEN);
		action.setTargetNode("general");
		action.buildTree();
		Set<String> targetsToOpen = action.getTreeNodesToOpen();
		Assertions.assertTrue(targetsToOpen.size()> 0);
		action.setTreeNodeActionMarkerCode(FacetNavTreeWidgetAction.ACTION_MARKER_CLOSE);
		action.buildTree();
		targetsToOpen = action.getTreeNodesToOpen();
		TreeNodeWrapper category = (TreeNodeWrapper) action.getShowableTree();
		Assertions.assertEquals("home", category.getCode());

		WidgetTypeParameter parameter = action.getWidgetTypeParameter("facetRootNodes");
		Assertions.assertNotNull(parameter);
	}

	@Test
    public void testAddRemoveContentType() throws Throwable {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("pageCode", "homepage");
		parameters.put("frame", "1");
		parameters.put("widgetTypeCode", "jpsolr_facetTree");
        
        parameters.put("contentTypeCode", "EVN");
		String result = this.executeAction("admin", "/do/jpsolr/Page/SpecialWidget/FacetNavTree", "joinContentType", parameters);
		assertEquals(Action.SUCCESS, result);
		FacetNavTreeWidgetAction action = (FacetNavTreeWidgetAction) this.getAction();
		List<SmallContentType> contentTypes = action.getContentTypes();
		assertNotNull(contentTypes);
		assertEquals(4, contentTypes.size());
        assertEquals("EVN", action.getContentTypesFilter());
        
        parameters.put("contentTypesFilter", "RAH,ART");
        parameters.put("contentTypeCode", "ALL");
		result = this.executeAction("admin", "/do/jpsolr/Page/SpecialWidget/FacetNavTree", "joinContentType", parameters);
		assertEquals(Action.SUCCESS, result);
		action = (FacetNavTreeWidgetAction) this.getAction();
        assertEquals("RAH,ART,ALL", action.getContentTypesFilter());
        
        parameters.put("contentTypeCode", "EVN");
        parameters.put("contentTypesFilter", "RAH,EVN,ART,ALL");
        result = this.executeAction("admin", "/do/jpsolr/Page/SpecialWidget/FacetNavTree", "removeContentType", parameters);
		assertEquals(Action.SUCCESS, result);
		action = (FacetNavTreeWidgetAction) this.getAction();
        assertEquals("RAH,ART,ALL", action.getContentTypesFilter());
	}
    
	@Test
    public void testAddRemoveFacets() throws Throwable {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("pageCode", "homepage");
		parameters.put("frame", "1");
		parameters.put("widgetTypeCode", "jpsolr_facetTree");
        parameters.put("contentTypesFilter", "RAH,EVN,ALL");
        
        parameters.put("facetCode", "cat1");
		String result = this.executeAction("admin", "/do/jpsolr/Page/SpecialWidget/FacetNavTree", "joinFacet", parameters);
		assertEquals(Action.SUCCESS, result);
		FacetNavTreeWidgetAction action = (FacetNavTreeWidgetAction) this.getAction();
		List<SmallContentType> contentTypes = action.getContentTypes();
		assertNotNull(contentTypes);
		assertEquals(4, contentTypes.size());
        assertEquals("cat1", action.getFacetRootNodes());
        assertEquals("RAH,EVN,ALL", action.getContentTypesFilter());
        
        parameters.put("facetCode", "general_cat2");
		parameters.put("facetRootNodes", "evento,general,cat1");
        parameters.put("contentTypesFilter", "RAH,ART,EVN");
		result = this.executeAction("admin", "/do/jpsolr/Page/SpecialWidget/FacetNavTree", "joinFacet", parameters);
		assertEquals(Action.SUCCESS, result);
		action = (FacetNavTreeWidgetAction) this.getAction();
        assertEquals("RAH,ART,EVN", action.getContentTypesFilter());
        assertEquals("evento,general,cat1,general_cat2", action.getFacetRootNodes());
        
        parameters.put("facetCode", "xxxx");
		parameters.put("facetRootNodes", "evento,general,cat1");
		parameters.put("contentTypesFilter", "RAH,ART,EVN");
		result = this.executeAction("admin", "/do/jpsolr/Page/SpecialWidget/FacetNavTree", "joinFacet", parameters);
		assertEquals(Action.SUCCESS, result);
		action = (FacetNavTreeWidgetAction) this.getAction();
        assertEquals("RAH,ART,EVN", action.getContentTypesFilter());
        assertEquals("evento,general,cat1", action.getFacetRootNodes());
        
        parameters.put("facetCode", "general");
		parameters.put("facetRootNodes", "evento,general,cat1");
		parameters.put("contentTypesFilter", "RAH,ART,EVN");
		result = this.executeAction("admin", "/do/jpsolr/Page/SpecialWidget/FacetNavTree", "removeFacet", parameters);
		assertEquals(Action.SUCCESS, result);
		action = (FacetNavTreeWidgetAction) this.getAction();
        assertEquals("RAH,ART,EVN", action.getContentTypesFilter());
        assertEquals("evento,cat1", action.getFacetRootNodes());
	}

    private String executeConfigFacetNavTree(String username, String pageCode, String frame, String widgetTypeCode) throws Throwable {
		this.setUserOnSession(username);
		this.initAction("/do/Page/SpecialWidget", "solrFacetNavTreeConfig");
		this.addParameter("pageCode", pageCode);
		this.addParameter("frame", frame);
		if (null != widgetTypeCode && widgetTypeCode.trim().length()>0) {
			this.addParameter("widgetTypeCode", widgetTypeCode);
		}
		return this.executeAction();
	}
	
    private String executeAction(String username, String namespace, String actionName, Map<String, String> parameters) throws Throwable {
		this.setUserOnSession(username);
		this.initAction(namespace, actionName);
		this.addParameters(parameters);
		return this.executeAction();
	}

	@Test
	void testSave() throws Throwable {
		this.testSave("", "", "customer_subpage_1", 0, "configure", 0, false, false); // to fix
		this.testSave("cat1", "", "customer_subpage_1", 0, "configure", 0, false, false);
		this.testSave("wrong", "", "customer_subpage_1", 0, Action.INPUT, 1, true, false);
		this.testSave("cat1", "xxxx", "customer_subpage_1", 0, Action.INPUT, 1, false, true);
		this.testSave("cat1,wrong", "ART", "customer_subpage_1", 0, Action.INPUT, 1, true, false);
		this.testSave("cat1", "ART", "customer_subpage_1", 0, "configure", 0, false, false);
		this.testSave("cat1,general", "ART,xxx", "customer_subpage_1", 0, Action.INPUT, 1, false, true);
		this.testSave("general,Image,cat1", "ART,ALL,EVN", "customer_subpage_1", 0, "configure", 0, false, false);
		this.testSave("general,xxxx,cat1", "ART,WRW,EVN", "customer_subpage_1", 0, Action.INPUT, 2, true, true);
	}

	private void testSave(String facetRootNodes, String contentTypes, String pageCode, int frame,
			String expectedResult, int fieldErrors, boolean hasFacetErrors, boolean hasTypesErrors) throws Throwable {
		try {
			this.intSaveConfig(facetRootNodes, contentTypes, pageCode, frame);
			String result = this.executeAction();
			assertEquals(expectedResult, result);
			ActionSupport action = this.getAction();
			assertEquals(fieldErrors, action.getFieldErrors().size());
			if (hasTypesErrors) {
				assertEquals(1, action.getFieldErrors().get("contentTypesFilter").size());
			}
			if (hasFacetErrors) {
				assertEquals(1, action.getFieldErrors().get("facetRootNodes").size());
			}
		} catch (Throwable t) {
			throw t;
		} finally {
			IPage page = this.pageManager.getDraftPage(pageCode);
			page.getWidgets()[frame] = null;
			this.pageManager.updatePage(page);
		}
	}

	private void intSaveConfig(String facetRootNodes, String contentTypesCsv, String pageCode, int frame) throws Throwable {
		IPage page = this.pageManager.getDraftPage(pageCode);
		Widget widget = page.getWidgets()[frame];
		Assertions.assertNull(widget);
		this.setUserOnSession("admin");
		this.initAction("/do/jpsolr/Page/SpecialWidget/FacetNavTree", "saveConfig");
		Map<String, String> params = new HashMap<>();
		params.put("pageCode", pageCode);
		params.put("frame", String.valueOf(frame));
		params.put("widgetTypeCode", "jpsolr_facetTree");
		if (StringUtils.isNotBlank(contentTypesCsv)) {
			params.put("contentTypesFilter", contentTypesCsv);
		}
		if (StringUtils.isNotBlank(facetRootNodes)) {
			params.put("facetRootNodes", facetRootNodes);
		}
		this.addParameters(params);
	}
    
}