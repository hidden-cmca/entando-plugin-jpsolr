package org.entando.entando.plugins.jpsolr.aps.tags;

import com.agiletec.aps.system.RequestContext;
import com.agiletec.aps.system.SystemConstants;
import com.agiletec.aps.system.common.tree.ITreeNodeManager;
import com.agiletec.aps.system.common.tree.TreeNode;
import com.agiletec.aps.system.services.page.Widget;
import com.agiletec.aps.util.ApsProperties;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import org.entando.entando.aps.system.services.searchengine.FacetedContentsResult;
import org.entando.entando.aps.system.services.widgettype.WidgetType;
import org.entando.entando.plugins.jpsolr.aps.system.JpSolrSystemConstants;
import org.entando.entando.plugins.jpsolr.aps.system.content.widget.IFacetNavHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@ExtendWith(MockitoExtension.class)
public class FacetNavTreeTagTest {

    @Mock
    private PageContext pageContext;
    @Mock
    private IFacetNavHelper facetNavHelper;
    @Mock
    private ITreeNodeManager treeNodeManager;
    @Mock
    private RequestContext reqCtx;
    @Mock
    private ServletRequest request;
    @Mock
    private TreeNode node;
    @InjectMocks
    private FacetNavTreeTag facetNavTreeTag;

    @BeforeEach
    public void setUp() throws Exception {
        Widget widget = new Widget();
        widget.setType(new WidgetType());
        ApsProperties properties = new ApsProperties();
        properties.setProperty(JpSolrSystemConstants.FACET_ROOTS_WIDGET_PARAM_NAME, "images,general");
        widget.setConfig(properties);
        Mockito.lenient().when(reqCtx.getExtraParam(SystemConstants.EXTRAPAR_CURRENT_WIDGET)).thenReturn(widget);
        Mockito.when(pageContext.getRequest()).thenReturn(request);
        Mockito.when(request.getAttribute(RequestContext.REQCTX)).thenReturn(reqCtx);
        List<String> params = Arrays.asList(new String[]{"param1"});
        Enumeration e = Collections.enumeration(params);
        Mockito.lenient().when(request.getParameterNames()).thenReturn(e);
        Mockito.lenient().when(request.getParameterValues("param1")).thenReturn(new String[]{"value1"});
        Mockito.lenient().when(facetNavHelper.getTreeNodeManager()).thenReturn(this.treeNodeManager);
        Mockito.lenient().when(facetNavHelper.getResult(Mockito.anyList(), Mockito.eq(reqCtx))).thenReturn(Mockito.mock(FacetedContentsResult.class));
        Mockito.lenient().when(treeNodeManager.getNode(Mockito.anyString())).thenReturn(node);
    }

    @Test
    public void testDoStartTag_1() throws Exception {
        try (MockedStatic<WebApplicationContextUtils> staticWebAppCont = Mockito.mockStatic(WebApplicationContextUtils.class)) {
            ServletContext svCtx = Mockito.mock(ServletContext.class);
            Mockito.when(pageContext.getServletContext()).thenReturn(svCtx);
            WebApplicationContext webApplicationContext = Mockito.mock(WebApplicationContext.class);
            staticWebAppCont.when(() -> WebApplicationContextUtils.getWebApplicationContext(svCtx)).thenReturn(webApplicationContext);
            Mockito.when(webApplicationContext.getBean(JpSolrSystemConstants.CONTENT_FACET_NAV_HELPER)).thenReturn(facetNavHelper);
            facetNavTreeTag.setFacetsTreeParamName("facetsParamNameVar");
            facetNavTreeTag.setOccurrencesParamName("occurrencesParamNameVar");
            int result = facetNavTreeTag.doStartTag();
            Assertions.assertEquals(0, result);
            Mockito.verify(this.facetNavHelper, Mockito.times(1)).getResult(Mockito.anyList(), Mockito.eq(reqCtx));
            Mockito.verify(this.request, Mockito.times(2)).setAttribute(Mockito.any(), Mockito.any());
        }
    }

    @Test
    public void testDoStartTag_2() throws Exception {
        try (MockedStatic<WebApplicationContextUtils> staticWebAppCont = Mockito.mockStatic(WebApplicationContextUtils.class)) {
            ServletContext svCtx = Mockito.mock(ServletContext.class);
            Mockito.when(pageContext.getServletContext()).thenReturn(svCtx);
            WebApplicationContext webApplicationContext = Mockito.mock(WebApplicationContext.class);
            staticWebAppCont.when(() -> WebApplicationContextUtils.getWebApplicationContext(svCtx)).thenReturn(webApplicationContext);
            Mockito.when(webApplicationContext.getBean(JpSolrSystemConstants.CONTENT_FACET_NAV_HELPER)).thenReturn(facetNavHelper);
            Mockito.when(request.getParameter("facetNode_1")).thenReturn("facet1");
            Mockito.when(request.getParameter("facetNode_2")).thenReturn("facet2");
            Mockito.when(request.getParameter("facetNode_3")).thenReturn("facet3");
            Mockito.when(request.getParameter("facetNode_4")).thenReturn("facet4");
            Mockito.when(request.getParameter("facetNode_5")).thenReturn(null);
            Mockito.lenient().when(node.getCode()).thenReturn("code");
            Mockito.lenient().when(node.getParentCode()).thenReturn("parentCode").thenReturn("code");
            Mockito.when(request.getParameter("selectedNode")).thenReturn("facet5");
            Mockito.lenient().when(request.getParameterValues("facetNodeToRemove")).thenReturn(new String[]{"value4", "facet4"});
            Mockito.lenient().when(request.getParameter("facetNodeToRemove_1")).thenReturn("facet2");
            int result = facetNavTreeTag.doStartTag();
            Assertions.assertEquals(0, result);
            Mockito.verify(this.facetNavHelper, Mockito.times(1)).getResult(Mockito.anyList(), Mockito.eq(reqCtx));
            Mockito.verify(this.request, Mockito.times(2)).setAttribute(Mockito.any(), Mockito.any());
            ArgumentCaptor<List> argument = ArgumentCaptor.forClass(List.class);
            Mockito.verify(this.facetNavHelper).getResult(argument.capture(), Mockito.eq(reqCtx));
            Assertions.assertEquals(3, argument.getValue().size());
        }
    }

    @Test
    public void testDoStartTag_3() throws Exception {
        Mockito.doThrow(RuntimeException.class).when(this.pageContext).getServletContext();
        Assertions.assertThrows(JspException.class, () -> {
            int result = facetNavTreeTag.doStartTag();
        });
    }

}
