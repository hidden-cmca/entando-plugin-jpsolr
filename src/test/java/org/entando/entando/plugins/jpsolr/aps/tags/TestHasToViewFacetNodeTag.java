package org.entando.entando.plugins.jpsolr.aps.tags;

import com.agiletec.aps.system.RequestContext;
import com.agiletec.aps.system.SystemConstants;
import com.agiletec.aps.system.common.tree.ITreeNodeManager;
import com.agiletec.aps.system.common.tree.TreeNode;
import com.agiletec.aps.system.services.lang.ILangManager;
import com.agiletec.aps.system.services.lang.Lang;
import com.agiletec.aps.system.services.page.IPage;
import com.agiletec.aps.util.ApsProperties;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import org.entando.entando.plugins.jpsolr.aps.system.JpSolrSystemConstants;
import org.entando.entando.plugins.jpsolr.aps.system.content.widget.IFacetNavHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@ExtendWith(MockitoExtension.class)
public class TestHasToViewFacetNodeTag {

    @Mock
    private PageContext pageContext;
    @Mock
    private IFacetNavHelper facetNavHelper;
    @Mock
    private ITreeNodeManager treeNodeManager;
    @Mock
    private RequestContext reqCtx;
    @Mock
    private ServletRequest request;
    @Mock
    private TreeNode node;
    @InjectMocks
    private HasToViewFacetNodeTag hasToViewFacetNodeTag;

    @BeforeEach
    public void setUp() throws Exception {
        Mockito.when(pageContext.getRequest()).thenReturn(request);
        Mockito.lenient().when(request.getAttribute(RequestContext.REQCTX)).thenReturn(reqCtx);
        Mockito.lenient().when(facetNavHelper.getTreeNodeManager()).thenReturn(this.treeNodeManager);
        Mockito.lenient().when(treeNodeManager.getNode(Mockito.anyString())).thenReturn(node);
        Enumeration e = Collections.enumeration(Arrays.asList(new String[]{"param1"}));
        Mockito.lenient().when(request.getParameterNames()).thenReturn(e);
        hasToViewFacetNodeTag.release();
        hasToViewFacetNodeTag.setFacetNodeCode("facetCode");
    }

    @Test
    public void testDoStartTag_1() throws Exception {
        try (MockedStatic<WebApplicationContextUtils> staticWebAppCont = Mockito.mockStatic(WebApplicationContextUtils.class)) {
            ServletContext svCtx = Mockito.mock(ServletContext.class);
            Mockito.when(pageContext.getServletContext()).thenReturn(svCtx);
            WebApplicationContext webApplicationContext = Mockito.mock(WebApplicationContext.class);
            staticWebAppCont.when(() -> WebApplicationContextUtils.getWebApplicationContext(svCtx)).thenReturn(webApplicationContext);
            Mockito.when(webApplicationContext.getBean(JpSolrSystemConstants.CONTENT_FACET_NAV_HELPER)).thenReturn(facetNavHelper);
            int result = hasToViewFacetNodeTag.doStartTag();
            Assertions.assertEquals(0, result);
            Mockito.verify(this.treeNodeManager, Mockito.times(1)).getNode(Mockito.any());
            Mockito.verify(this.request, Mockito.never()).getAttribute(Mockito.anyString());
        }
    }

    @Test
    public void testDoStartTag_2() throws Exception {
        try (MockedStatic<WebApplicationContextUtils> staticWebAppCont = Mockito.mockStatic(WebApplicationContextUtils.class)) {
            ServletContext svCtx = Mockito.mock(ServletContext.class);
            Mockito.when(pageContext.getServletContext()).thenReturn(svCtx);
            WebApplicationContext webApplicationContext = Mockito.mock(WebApplicationContext.class);
            staticWebAppCont.when(() -> WebApplicationContextUtils.getWebApplicationContext(svCtx)).thenReturn(webApplicationContext);
            Mockito.when(webApplicationContext.getBean(JpSolrSystemConstants.CONTENT_FACET_NAV_HELPER)).thenReturn(facetNavHelper);
            hasToViewFacetNodeTag.setFacetNodeCode(null);
            int result = hasToViewFacetNodeTag.doStartTag();
            Assertions.assertEquals(0, result);
            Mockito.verify(this.treeNodeManager, Mockito.never()).getNode(Mockito.any());
            Mockito.verify(this.request, Mockito.never()).setAttribute(Mockito.any(), Mockito.any());
        }
    }


    @Test
    public void testDoStartTag_3() throws Exception {
        try (MockedStatic<WebApplicationContextUtils> staticWebAppCont = Mockito.mockStatic(WebApplicationContextUtils.class)) {
            ServletContext svCtx = Mockito.mock(ServletContext.class);
            Mockito.when(pageContext.getServletContext()).thenReturn(svCtx);
            WebApplicationContext webApplicationContext = Mockito.mock(WebApplicationContext.class);
            staticWebAppCont.when(() -> WebApplicationContextUtils.getWebApplicationContext(svCtx)).thenReturn(webApplicationContext);
            Mockito.when(webApplicationContext.getBean(JpSolrSystemConstants.CONTENT_FACET_NAV_HELPER)).thenReturn(facetNavHelper);
            hasToViewFacetNodeTag.setFacetNodeCode(null);
            hasToViewFacetNodeTag.setRequiredFacetsParamName("facetParam");
            int result = hasToViewFacetNodeTag.doStartTag();
            Assertions.assertEquals(0, result);
            Mockito.verify(this.treeNodeManager, Mockito.never()).getNode(Mockito.any());
            Mockito.verify(this.request, Mockito.atLeast(1)).getAttribute("facetParam");
            Mockito.verify(this.request, Mockito.never()).setAttribute(Mockito.any(), Mockito.any());
        }
    }

    @Test
    public void testDoStartTag_4() throws Exception {
        try (MockedStatic<WebApplicationContextUtils> staticWebAppCont = Mockito.mockStatic(WebApplicationContextUtils.class)) {
            ServletContext svCtx = Mockito.mock(ServletContext.class);
            Mockito.when(pageContext.getServletContext()).thenReturn(svCtx);
            WebApplicationContext webApplicationContext = Mockito.mock(WebApplicationContext.class);
            staticWebAppCont.when(() -> WebApplicationContextUtils.getWebApplicationContext(svCtx)).thenReturn(webApplicationContext);
            Mockito.when(webApplicationContext.getBean(JpSolrSystemConstants.CONTENT_FACET_NAV_HELPER)).thenReturn(facetNavHelper);

            Mockito.when(request.getParameter("facetNode_1")).thenReturn("facet1");
            Mockito.when(request.getParameter("facetNode_2")).thenReturn("facet2");
            Mockito.when(request.getParameter("facetNode_3")).thenReturn(null);
            Mockito.lenient().when(node.getCode()).thenReturn("code");
            Mockito.lenient().when(node.getParentCode()).thenReturn("parentCode").thenReturn("code");

            int result = hasToViewFacetNodeTag.doStartTag();
            Assertions.assertEquals(0, result);
            Mockito.verify(this.treeNodeManager, Mockito.atLeast(4)).getNode(Mockito.any());
            Mockito.verify(this.request, Mockito.never()).setAttribute(Mockito.any(), Mockito.any());
        }
    }

    @Test
    public void testDoStartTag_5() throws Exception {
        Mockito.doThrow(RuntimeException.class).when(this.pageContext).getServletContext();
        Assertions.assertThrows(RuntimeException.class, () -> {
            int result = hasToViewFacetNodeTag.doStartTag();
        });
    }

}
