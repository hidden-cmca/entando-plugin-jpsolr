package org.entando.entando.plugins.jpsolr.aps.tags;

import com.agiletec.aps.system.RequestContext;
import com.agiletec.aps.system.SystemConstants;
import com.agiletec.aps.system.common.tree.ITreeNodeManager;
import com.agiletec.aps.system.common.tree.TreeNode;
import com.agiletec.aps.system.services.lang.ILangManager;
import com.agiletec.aps.system.services.lang.Lang;
import com.agiletec.aps.system.services.page.IPage;
import com.agiletec.aps.util.ApsProperties;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import org.entando.entando.plugins.jpsolr.aps.system.JpSolrSystemConstants;
import org.entando.entando.plugins.jpsolr.aps.system.content.widget.IFacetNavHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@ExtendWith(MockitoExtension.class)
public class TestFacetNodeTitleTag {

    @Mock
    private PageContext pageContext;
    @Mock
    private IFacetNavHelper facetNavHelper;
    @Mock
    private ITreeNodeManager treeNodeManager;
    @Mock
    private ILangManager langManager;
    @Mock
    private RequestContext reqCtx;
    @Mock
    private ServletRequest request;
    @Mock
    private TreeNode node;
    @InjectMocks
    private FacetNodeTitleTag facetNodeTitleTag;

    @BeforeEach
    public void setUp() throws Exception {
        Lang currentLang = new Lang();
        currentLang.setCode("en");
        Mockito.lenient().when(reqCtx.getExtraParam(SystemConstants.EXTRAPAR_CURRENT_LANG)).thenReturn(currentLang);
        Lang defaultLang = new Lang();
        defaultLang.setCode("it");
        Mockito.lenient().when(langManager.getDefaultLang()).thenReturn(defaultLang);
        Mockito.when(pageContext.getRequest()).thenReturn(request);
        Mockito.when(request.getAttribute(RequestContext.REQCTX)).thenReturn(reqCtx);
        Mockito.lenient().when(facetNavHelper.getTreeNodeManager()).thenReturn(this.treeNodeManager);
        ApsProperties titles = new ApsProperties();
        titles.put("it", "it Title");
        titles.put("en", "en Title");
        Mockito.lenient().when(treeNodeManager.getNode(Mockito.anyString())).thenReturn(node);
        Mockito.lenient().when(node.getTitles()).thenReturn(titles);
        Mockito.lenient().when(pageContext.getOut()).thenReturn(Mockito.mock(JspWriter.class));
        facetNodeTitleTag.release();
    }

    @Test
    public void testDoStartTag_1() throws Exception {
        try (MockedStatic<WebApplicationContextUtils> staticWebAppCont = Mockito.mockStatic(WebApplicationContextUtils.class)) {
            ServletContext svCtx = Mockito.mock(ServletContext.class);
            Mockito.when(pageContext.getServletContext()).thenReturn(svCtx);
            WebApplicationContext webApplicationContext = Mockito.mock(WebApplicationContext.class);
            staticWebAppCont.when(() -> WebApplicationContextUtils.getWebApplicationContext(svCtx)).thenReturn(webApplicationContext);
            Mockito.when(webApplicationContext.getBean(JpSolrSystemConstants.CONTENT_FACET_NAV_HELPER)).thenReturn(facetNavHelper);
            facetNodeTitleTag.setFullTitle(false);
            facetNodeTitleTag.setFacetNodeCode("facetCode");
            int result = facetNodeTitleTag.doStartTag();
            Assertions.assertEquals(0, result);
            Mockito.verify(this.request, Mockito.never()).setAttribute(Mockito.any(), Mockito.any());
            Mockito.verifyNoInteractions(this.langManager);
        }
    }

    @Test
    public void testDoStartTag_2() throws Exception {
        try (MockedStatic<WebApplicationContextUtils> staticWebAppCont = Mockito.mockStatic(WebApplicationContextUtils.class)) {
            ServletContext svCtx = Mockito.mock(ServletContext.class);
            Mockito.when(pageContext.getServletContext()).thenReturn(svCtx);
            WebApplicationContext webApplicationContext = Mockito.mock(WebApplicationContext.class);
            staticWebAppCont.when(() -> WebApplicationContextUtils.getWebApplicationContext(svCtx)).thenReturn(webApplicationContext);
            Mockito.when(webApplicationContext.getBean(JpSolrSystemConstants.CONTENT_FACET_NAV_HELPER)).thenReturn(facetNavHelper);
            Mockito.when(webApplicationContext.getBean(SystemConstants.LANGUAGE_MANAGER)).thenReturn(langManager);
            Mockito.when(pageContext.getOut()).thenReturn(Mockito.mock(JspWriter.class));
            Mockito.when(node.getFullTitle(Mockito.anyString(), Mockito.anyString(), Mockito.eq(treeNodeManager))).thenReturn(null);
            facetNodeTitleTag.setFacetNodeCode("facetCode");
            facetNodeTitleTag.setFullTitle(true);
            facetNodeTitleTag.setSeparator(null);
            facetNodeTitleTag.setEscapeXml(false);
            int result = facetNodeTitleTag.doStartTag();
            Assertions.assertEquals(0, result);
            Mockito.verify(this.langManager, Mockito.times(1)).getDefaultLang();
            Mockito.verify(this.node, Mockito.times(2)).getFullTitle(Mockito.anyString(), Mockito.eq(" / "), Mockito.eq(treeNodeManager));
        }
    }

    @Test
    public void testDoStartTag_3() throws Exception {
        try (MockedStatic<WebApplicationContextUtils> staticWebAppCont = Mockito.mockStatic(WebApplicationContextUtils.class)) {
            ServletContext svCtx = Mockito.mock(ServletContext.class);
            Mockito.when(pageContext.getServletContext()).thenReturn(svCtx);
            WebApplicationContext webApplicationContext = Mockito.mock(WebApplicationContext.class);
            staticWebAppCont.when(() -> WebApplicationContextUtils.getWebApplicationContext(svCtx)).thenReturn(webApplicationContext);
            Mockito.when(webApplicationContext.getBean(JpSolrSystemConstants.CONTENT_FACET_NAV_HELPER)).thenReturn(facetNavHelper);
            facetNodeTitleTag.setFacetNodeCode(null);
            int result = facetNodeTitleTag.doStartTag();
            Assertions.assertEquals(0, result);
            Mockito.verify(this.request, Mockito.never()).setAttribute(Mockito.any(), Mockito.any());
            Mockito.verifyNoInteractions(this.langManager);
            Mockito.verifyNoInteractions(this.node);

        }
    }

    @Test
    public void testDoStartTag_4() throws Exception {
        Mockito.doThrow(RuntimeException.class).when(this.pageContext).getServletContext();
        Assertions.assertThrows(JspException.class, () -> {
            int result = facetNodeTitleTag.doStartTag();
        });
    }

}
