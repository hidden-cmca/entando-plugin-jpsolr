package org.entando.entando.plugins.jpsolr.aps.tags;

import com.agiletec.aps.system.common.tree.ITreeNodeManager;
import com.agiletec.aps.system.common.tree.TreeNode;
import java.util.Arrays;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import org.entando.entando.plugins.jpsolr.aps.system.JpSolrSystemConstants;
import org.entando.entando.plugins.jpsolr.aps.system.content.widget.IFacetNavHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@ExtendWith(MockitoExtension.class)
public class TestHasToOpenFacetNodeTag {

    @Mock
    private PageContext pageContext;
    @Mock
    private IFacetNavHelper facetNavHelper;
    @Mock
    private ITreeNodeManager treeNodeManager;
    @Mock
    private ServletRequest request;
    @Mock
    private TreeNode node;
    @InjectMocks
    private HasToOpenFacetNodeTag hasToOpenFacetNodeTag;

    @BeforeEach
    public void setUp() throws Exception {
        Mockito.lenient().when(facetNavHelper.getTreeNodeManager()).thenReturn(this.treeNodeManager);
        hasToOpenFacetNodeTag.release();
        hasToOpenFacetNodeTag.setFacetNodeCode("facetCode");
    }

    @Test
    public void testDoStartTag_1() throws Exception {
        try (MockedStatic<WebApplicationContextUtils> staticWebAppCont = Mockito.mockStatic(WebApplicationContextUtils.class)) {
            ServletContext svCtx = Mockito.mock(ServletContext.class);
            Mockito.when(pageContext.getServletContext()).thenReturn(svCtx);
            WebApplicationContext webApplicationContext = Mockito.mock(WebApplicationContext.class);
            staticWebAppCont.when(() -> WebApplicationContextUtils.getWebApplicationContext(svCtx)).thenReturn(webApplicationContext);
            Mockito.when(webApplicationContext.getBean(JpSolrSystemConstants.CONTENT_FACET_NAV_HELPER)).thenReturn(facetNavHelper);
            int result = hasToOpenFacetNodeTag.doStartTag();
            Assertions.assertEquals(0, result);
            Mockito.verify(this.treeNodeManager, Mockito.never()).getNode(Mockito.any());
            Mockito.lenient().when(treeNodeManager.getNode(Mockito.anyString())).thenReturn(node);
        }
    }

    @Test
    public void testDoStartTag_2() throws Exception {
        try (MockedStatic<WebApplicationContextUtils> staticWebAppCont = Mockito.mockStatic(WebApplicationContextUtils.class)) {
            ServletContext svCtx = Mockito.mock(ServletContext.class);
            Mockito.when(pageContext.getServletContext()).thenReturn(svCtx);
            WebApplicationContext webApplicationContext = Mockito.mock(WebApplicationContext.class);
            staticWebAppCont.when(() -> WebApplicationContextUtils.getWebApplicationContext(svCtx)).thenReturn(webApplicationContext);
            Mockito.when(webApplicationContext.getBean(JpSolrSystemConstants.CONTENT_FACET_NAV_HELPER)).thenReturn(facetNavHelper);
            hasToOpenFacetNodeTag.setRequiredFacetsParamName("requiredFacetParam");
            Mockito.when(pageContext.getRequest()).thenReturn(request);
            Mockito.when(request.getAttribute("requiredFacetParam")).thenReturn(Arrays.asList(new String[]{"facet1", "facet2"}));
            Mockito.lenient().when(treeNodeManager.getNode(Mockito.anyString())).thenReturn(node);
            Mockito.lenient().when(node.getCode()).thenReturn("code").thenReturn("otherCode");
            Mockito.lenient().when(node.getParentCode()).thenReturn("parentCode").thenReturn("code");
            Mockito.lenient().when(node.getChildrenCodes()).thenReturn(new String[]{"children1", "children2"}).thenReturn(new String[]{"children4", "children3"});

            hasToOpenFacetNodeTag.setFacetNodeCode("code");
            int result = hasToOpenFacetNodeTag.doStartTag();
            Assertions.assertEquals(0, result);
            Mockito.verify(this.treeNodeManager, Mockito.atLeast(2)).getNode(Mockito.any());
        }
    }

    @Test
    public void testDoStartTag_3() throws Exception {
        Mockito.doThrow(RuntimeException.class).when(this.pageContext).getServletContext();
        Assertions.assertThrows(JspException.class, () -> {
            int result = hasToOpenFacetNodeTag.doStartTag();
        });
    }

}
