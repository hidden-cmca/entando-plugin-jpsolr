package org.entando.entando.plugins.jpsolr.aps.system.content;

import com.agiletec.aps.BaseTestCase;
import com.agiletec.aps.system.common.entity.model.IApsEntity;
import com.agiletec.aps.system.services.group.Group;
import com.agiletec.aps.util.ApsProperties;
import com.agiletec.plugins.jacms.aps.system.services.content.IContentManager;
import com.agiletec.plugins.jacms.aps.system.services.content.widget.UserFilterOptionBean;
import com.agiletec.plugins.jacms.aps.system.services.searchengine.ICmsSearchEngineManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.entando.entando.aps.system.services.searchengine.FacetedContentsResult;
import org.entando.entando.aps.system.services.searchengine.SearchEngineFilter;
import org.entando.entando.ent.exception.EntException;
import org.entando.entando.plugins.jpsolr.aps.system.PluginBaseTestCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AdvContentFacetManagerIntegrationTest extends PluginBaseTestCase {

    private IAdvContentFacetManager contentFacetManager;

    private List<String> allowedGroup = new ArrayList<>();

    @BeforeEach
    protected void init() throws Exception {
        try {
            this.contentFacetManager = PluginBaseTestCase.getApplicationContext().getBean(IAdvContentFacetManager.class);
            ICmsSearchEngineManager searchEngineManager = PluginBaseTestCase.getApplicationContext().getBean(ICmsSearchEngineManager.class);
            Thread thread = searchEngineManager.startReloadContentsReferences();
            thread.join();
            allowedGroup.add(Group.ADMINS_GROUP_NAME);
        } catch (Exception e) {
            throw e;
        }
    }

    @Test
    public void testSearchContents_1() throws Throwable {
        SearchEngineFilter groupFilter = new SearchEngineFilter(IContentManager.CONTENT_MAIN_GROUP_FILTER_KEY, false, "coach", SearchEngineFilter.TextSearchOption.EXACT);
        SearchEngineFilter[] filters = {groupFilter};
        List<String> facetNodeCodes = new ArrayList<>();
        List<UserFilterOptionBean> userFiltersBeans = new ArrayList<>();
        FacetedContentsResult result = this.contentFacetManager.getFacetResult(filters, facetNodeCodes, userFiltersBeans, this.allowedGroup);
        Assertions.assertNotNull(result);
        List<String> contentIds = result.getContentsId();
        String[] expected = {"EVN103", "ART104", "ART111", "ART112", "EVN25", "EVN41"};
        Assertions.assertEquals(expected.length, contentIds.size());
        for (int i = 0; i < expected.length; i++) {
            String contentId = expected[i];
            Assertions.assertTrue(contentIds.contains(contentId));
        }
    }

    @Test
    public void testSearchContents_2() throws Throwable {
        SearchEngineFilter descrFilter = new SearchEngineFilter(IContentManager.CONTENT_DESCR_FILTER_KEY, false, "Mostra della ciliegia", SearchEngineFilter.TextSearchOption.EXACT);
        SearchEngineFilter[] filters = {descrFilter};
        List<String> facetNodeCodes = new ArrayList<>();
        List<UserFilterOptionBean> userFiltersBeans = new ArrayList<>();
        FacetedContentsResult result = this.contentFacetManager.getFacetResult(filters, facetNodeCodes, userFiltersBeans, this.allowedGroup);
        Assertions.assertNotNull(result);
        List<String> contentIds = result.getContentsId();
        String[] expected = {"EVN41"};
        Assertions.assertEquals(expected.length, contentIds.size());
        for (int i = 0; i < expected.length; i++) {
            String contentId = expected[i];
            Assertions.assertTrue(contentIds.contains(contentId));
        }
    }

    @Test
    public void testSearchContents_3() throws Throwable {
        SearchEngineFilter attributeFilter = new SearchEngineFilter("Titolo", true, "Sagra della ciliegia",
                SearchEngineFilter.TextSearchOption.EXACT);
        attributeFilter.setLangCode("it");
        SearchEngineFilter[] filters = {attributeFilter};
        List<String> facetNodeCodes = new ArrayList<>();
        List<UserFilterOptionBean> userFiltersBeans = new ArrayList<>();
        FacetedContentsResult result = this.contentFacetManager
                .getFacetResult(filters, facetNodeCodes, userFiltersBeans, this.allowedGroup);
        Assertions.assertNotNull(result);
        List<String> contentIds = result.getContentsId();
        String[] expected = {"EVN41"};
        Assertions.assertEquals(expected.length, contentIds.size());
        for (int i = 0; i < expected.length; i++) {
            String contentId = expected[i];
            Assertions.assertTrue(contentIds.contains(contentId));
        }
    }

    @Test
    public void testLoadPublicContentsForCategory_1() throws EntException {
        SearchEngineFilter[] filters = {};
        List<UserFilterOptionBean> userFiltersBeans = new ArrayList<>();
        List<String> facetNodeCodes_1 = new ArrayList<>();
        facetNodeCodes_1.add("evento");
        FacetedContentsResult result = this.contentFacetManager.getFacetResult(filters, facetNodeCodes_1, userFiltersBeans, this.allowedGroup);
        List<String> contents = result.getContentsId();
        Assertions.assertEquals(2, contents.size());
        Assertions.assertTrue(contents.contains("EVN192"));
        Assertions.assertTrue(contents.contains("EVN193"));

        List<String> facetNodeCodes_2 = new ArrayList<>();
        facetNodeCodes_2.add("cat1");
        result = this.contentFacetManager.getFacetResult(filters, facetNodeCodes_2, userFiltersBeans, this.allowedGroup);
        contents = result.getContentsId();
        Assertions.assertEquals(1, contents.size());
        Assertions.assertTrue(contents.contains("ART180"));
    }

    @Test
    public void testLoadPublicContentsForCategory_2() throws EntException {
        SearchEngineFilter[] filters = {};
        List<UserFilterOptionBean> userFiltersBeans = new ArrayList<>();
        SearchEngineFilter categoryFilter_1 = new SearchEngineFilter("category", false, "evento");
        SearchEngineFilter[] facet_1 = new SearchEngineFilter[]{categoryFilter_1};
        FacetedContentsResult result = this.contentFacetManager.getFacetResult(filters, facet_1, userFiltersBeans, this.allowedGroup);
        List<String> contents = result.getContentsId();
        Assertions.assertEquals(2, contents.size());
        Assertions.assertTrue(contents.contains("EVN192"));
        Assertions.assertTrue(contents.contains("EVN193"));

        SearchEngineFilter categoryFilter_2 = new SearchEngineFilter("category", false, "cat1");
        SearchEngineFilter[] facet_2 = new SearchEngineFilter[]{categoryFilter_2};
        result = this.contentFacetManager.getFacetResult(filters, facet_2, userFiltersBeans, this.allowedGroup);
        contents = result.getContentsId();
        Assertions.assertEquals(1, contents.size());
        Assertions.assertTrue(contents.contains("ART180"));

        List<String> contentsId = this.contentFacetManager.loadContentsId(filters, facet_2, userFiltersBeans, this.allowedGroup);
        Assertions.assertEquals(1, contentsId.size());
        Assertions.assertTrue(contentsId.contains("ART180"));
    }

    @Test
    public void testLoadPublicContentsForCategory_3() throws Throwable {
        SearchEngineFilter[] filters = {};
        List<UserFilterOptionBean> userFiltersBeans = new ArrayList<>();
        ApsProperties properties = new ApsProperties();
        properties.setProperty("key", "category");
        userFiltersBeans.add(new CustomUserFilterOptionBean(properties, null));
        SearchEngineFilter[] facet = new SearchEngineFilter[]{};
        FacetedContentsResult result = this.contentFacetManager.getFacetResult(filters, facet, userFiltersBeans, this.allowedGroup);
        List<String> contents = result.getContentsId();
        Assertions.assertEquals(2, contents.size());
        Assertions.assertTrue(contents.contains("EVN192"));
        Assertions.assertTrue(contents.contains("EVN193"));
    }

    public class CustomUserFilterOptionBean extends UserFilterOptionBean {

        public CustomUserFilterOptionBean(Properties properties, IApsEntity prototype) throws Throwable {
            super(properties, null);
        }

        @Override
        public SearchEngineFilter extractFilter() {
            return new SearchEngineFilter("category", false, "evento");
        }
    }

}
