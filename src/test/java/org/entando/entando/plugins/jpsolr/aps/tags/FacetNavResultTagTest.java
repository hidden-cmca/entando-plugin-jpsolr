package org.entando.entando.plugins.jpsolr.aps.tags;

import com.agiletec.aps.system.RequestContext;
import com.agiletec.aps.system.SystemConstants;
import com.agiletec.aps.system.common.tree.ITreeNodeManager;
import com.agiletec.aps.system.common.tree.TreeNode;
import com.agiletec.aps.system.services.page.IPage;
import com.agiletec.aps.system.services.page.Widget;
import com.agiletec.aps.util.ApsProperties;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import org.entando.entando.aps.system.services.searchengine.FacetedContentsResult;
import org.entando.entando.aps.system.services.widgettype.WidgetType;
import org.entando.entando.plugins.jpsolr.aps.system.JpSolrSystemConstants;
import org.entando.entando.plugins.jpsolr.aps.system.content.widget.IFacetNavHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@ExtendWith(MockitoExtension.class)
public class FacetNavResultTagTest {

    @Mock
    private PageContext pageContext;
    @Mock
    private IFacetNavHelper facetNavHelper;
    @Mock
    private ITreeNodeManager treeNodeManager;
    @Mock
    private RequestContext reqCtx;
    @Mock
    private ServletRequest request;
    @InjectMocks
    private FacetNavResultTag facetNavResultTag;

    @BeforeEach
    public void setUp() throws Exception {
        IPage page = Mockito.mock(IPage.class);
        Widget widget = new Widget();
        widget.setType(new WidgetType());
        ApsProperties properties = new ApsProperties();
        properties.setProperty(JpSolrSystemConstants.FACET_ROOTS_WIDGET_PARAM_NAME, "root");
        widget.setConfig(properties);
        Widget[] widgets = new Widget[]{null, widget, null, widget, null, null};
        Mockito.lenient().when(page.getWidgets()).thenReturn(widgets);
        Mockito.lenient().when(reqCtx.getExtraParam(SystemConstants.EXTRAPAR_CURRENT_PAGE)).thenReturn(page);
        Mockito.lenient().when(reqCtx.getExtraParam(SystemConstants.EXTRAPAR_CURRENT_FRAME)).thenReturn(3);
        Mockito.when(pageContext.getRequest()).thenReturn(request);
        Mockito.when(request.getAttribute(RequestContext.REQCTX)).thenReturn(reqCtx);
        List<String> params = Arrays.asList(new String[]{"param1"});
        Enumeration e = Collections.enumeration(params);
        Mockito.lenient().when(request.getParameterNames()).thenReturn(e);
        Mockito.lenient().when(request.getParameterValues("param1")).thenReturn(new String[]{"value1"});
        Mockito.lenient().when(facetNavHelper.getTreeNodeManager()).thenReturn(this.treeNodeManager);
        Mockito.lenient().when(facetNavHelper.getResult(Mockito.anyList(), Mockito.eq(reqCtx))).thenReturn(Mockito.mock(FacetedContentsResult.class));
        Mockito.lenient().when(treeNodeManager.getNode(Mockito.anyString())).thenReturn(Mockito.mock(TreeNode.class));
    }

    @Test
    public void testDoStartTag_1() throws Exception {
        try (MockedStatic<WebApplicationContextUtils> staticWebAppCont = Mockito.mockStatic(WebApplicationContextUtils.class)) {
            ServletContext svCtx = Mockito.mock(ServletContext.class);
            Mockito.when(pageContext.getServletContext()).thenReturn(svCtx);
            WebApplicationContext webApplicationContext = Mockito.mock(WebApplicationContext.class);
            staticWebAppCont.when(() -> WebApplicationContextUtils.getWebApplicationContext(svCtx)).thenReturn(webApplicationContext);
            Mockito.when(webApplicationContext.getBean(JpSolrSystemConstants.CONTENT_FACET_NAV_HELPER)).thenReturn(facetNavHelper);
            facetNavResultTag.setResultParamName("result");
            int result = facetNavResultTag.doStartTag();
            Assertions.assertEquals(0, result);
            Mockito.verify(this.facetNavHelper, Mockito.times(1)).getResult(Mockito.anyList(), Mockito.eq(reqCtx));
        }
    }

    @Test
    public void testDoStartTag_2() throws Exception {
        try (MockedStatic<WebApplicationContextUtils> staticWebAppCont = Mockito.mockStatic(WebApplicationContextUtils.class)) {
            ServletContext svCtx = Mockito.mock(ServletContext.class);
            Mockito.when(pageContext.getServletContext()).thenReturn(svCtx);
            WebApplicationContext webApplicationContext = Mockito.mock(WebApplicationContext.class);
            staticWebAppCont.when(() -> WebApplicationContextUtils.getWebApplicationContext(svCtx)).thenReturn(webApplicationContext);
            Mockito.when(webApplicationContext.getBean(JpSolrSystemConstants.CONTENT_FACET_NAV_HELPER)).thenReturn(facetNavHelper);
            Mockito.when(request.getAttribute("facets")).thenReturn(new ArrayList<String>());
            facetNavResultTag.setExecuteExtractRequiredFacets(false);
            facetNavResultTag.setRequiredFacetsParamName("facets");
            facetNavResultTag.setBreadCrumbsParamName("breadcrumbs");
            int result = facetNavResultTag.doStartTag();
            Assertions.assertEquals(0, result);
            Mockito.verify(this.facetNavHelper, Mockito.times(1)).getResult(Mockito.anyList(), Mockito.eq(reqCtx));
            Mockito.verify(this.pageContext, Mockito.times(1)).setAttribute(Mockito.eq("breadcrumbs"), Mockito.any());
        }
    }

    @Test
    public void testDoStartTag_3() throws Exception {
        Mockito.doThrow(RuntimeException.class).when(this.pageContext).getServletContext();
        Assertions.assertThrows(JspException.class, () -> {
            int result = facetNavResultTag.doStartTag();
        });
    }

    @Test
    public void testDoStartTag_4() throws Exception {
        try (MockedStatic<WebApplicationContextUtils> staticWebAppCont = Mockito.mockStatic(WebApplicationContextUtils.class)) {
            ServletContext svCtx = Mockito.mock(ServletContext.class);
            Mockito.when(pageContext.getServletContext()).thenReturn(svCtx);
            WebApplicationContext webApplicationContext = Mockito.mock(WebApplicationContext.class);
            staticWebAppCont.when(() -> WebApplicationContextUtils.getWebApplicationContext(svCtx)).thenReturn(webApplicationContext);
            Mockito.when(webApplicationContext.getBean(JpSolrSystemConstants.CONTENT_FACET_NAV_HELPER)).thenReturn(facetNavHelper);
            Mockito.when(request.getParameter("facetNode_1")).thenReturn("facet1");
            Mockito.when(request.getParameter("facetNode_2")).thenReturn("facet2");
            Mockito.when(request.getParameter("facetNode_3")).thenReturn("facet3");
            Mockito.when(request.getParameter("facetNode_4")).thenReturn("facet4");
            Mockito.when(request.getParameter("facetNode_5")).thenReturn(null);
            TreeNode node = Mockito.mock(TreeNode.class);
            Mockito.lenient().when(treeNodeManager.getNode(Mockito.anyString())).thenReturn(node);
            Mockito.lenient().when(node.getCode()).thenReturn("code");
            Mockito.lenient().when(node.getParentCode()).thenReturn("parentCode").thenReturn("code");

            facetNavResultTag.setBreadCrumbsParamName("breadcrumbs");
            int result = facetNavResultTag.doStartTag();
            Assertions.assertEquals(0, result);
            Mockito.verify(this.facetNavHelper, Mockito.times(1)).getResult(Mockito.anyList(), Mockito.eq(reqCtx));
            Mockito.verify(this.pageContext, Mockito.times(1)).setAttribute(Mockito.eq("breadcrumbs"), Mockito.any());
        }
    }

}
